FROM python:3.7-alpine3.9

COPY docker-entrypoint.sh /

RUN    apk --no-cache add --virtual=.build-dep build-base \
    && apk --no-cache add zeromq-dev \
    && pip install --no-cache-dir locustio==0.8.1 paho-mqtt==1.4.0 python-dateutil==2.8.0 \
    && apk del .build-dep \
    && chmod +x /docker-entrypoint.sh

RUN  mkdir /locust
WORKDIR /locust
EXPOSE 8089 5557 5558

ENTRYPOINT ["/docker-entrypoint.sh"]