#!/bin/bash

function sudo_docker() {
  UNAME="$(uname -s)"
  if [[ $UNAME == 'Darwin' ]]; then
    echo "docker"
  elif [[ $UNAME == 'Linux'  ]]; then
    echo "sudo docker"
  else
    exit 1
  fi
}

if [[ ! ($# -eq 1) ]] ; then
    echo 'USAGE: docker.sh <mqtt or https>'
    exit 1
fi

# source: https://medium.com/locust-io-experiments/locust-io-experiments-running-in-docker-cae3c7f9386e

case `echo ${1} | tr 'a-z' 'A-Z'` in
"MQTT")
    ## standalone (mqtt only works standalone)
    echo 'Standalone: '
    echo "  $(sudo_docker) run --rm --name standalone --hostname standalone ..."
    "$(sudo_docker)" run --rm --name standalone --hostname standalone \
    -e ATTACKED_HOST="https://${STAGE}" \
    -v $(pwd)/mqtt/mqtt-locust/:/locust \
    -p 8089:8089 \
    -d grubykarol/locust:latest
    ;;

"HTTP" | "HTTPS")
    ## master-slaves
    echo 'Master-slaves:'
    echo '--------------'
    echo "Master: "
    echo "  $(sudo_docker) run --name master --hostname locust-master ..."
    "$(sudo_docker)" run --name master --hostname locust-master \
    -p 8089:8089 -p 5557:5557 -p 5558:5558 \
    -v $(pwd)/https:/locust \
    -e ATTACKED_HOST="https://${STAGE}" \
    -e LOCUST_MODE=master \
    --rm -d grubykarol/locust:latest

    echo "Slaves:"
    echo "  $(sudo_docker) run --name --name slave1 ..."
    "$(sudo_docker)" run --name slave1 \
    --link master --env NO_PROXY=master \
    -v $(pwd)/https:/locust \
    -e ATTACKED_HOST="https://${STAGE}" \
    -e LOCUST_MODE=slave \
    -e LOCUST_MASTER=locust-master \
    --rm -d grubykarol/locust:latest

    echo "  $(sudo_docker) run --name --name slave2 ..."
    "$(sudo_docker)" run --name slave2 \
    --link master --env NO_PROXY=master \
    -v $(pwd)/https:/locust \
    -e ATTACKED_HOST="https://${STAGE}" \
    -e LOCUST_MODE=slave \
    -e LOCUST_MASTER=locust-master \
    --rm -d grubykarol/locust:latest

    echo "  $(sudo_docker) run --name --name slave3 ..."
    "$(sudo_docker)" run --name slave3 \
    --link master --env NO_PROXY=master \
    -v $(pwd)/https:/locust \
    -e ATTACKED_HOST="https://${STAGE}" \
    -e LOCUST_MODE=slave \
    -e LOCUST_MASTER=locust-master \
    --rm -d grubykarol/locust:latest

    echo "  $(sudo_docker) run --name --name slave4 ..."
    "$(sudo_docker)" run --name slave4 \
    --link master --env NO_PROXY=master \
    -v $(pwd)/https:/locust \
    -e ATTACKED_HOST="https://${STAGE}" \
    -e LOCUST_MODE=slave \
    -e LOCUST_MASTER=locust-master \
    --rm -d grubykarol/locust:latest

    echo "  $(sudo_docker) run --name --name slave5 ..."
    "$(sudo_docker)" run --name slave5 \
    --link master --env NO_PROXY=master \
    -v $(pwd)/https:/locust \
    -e ATTACKED_HOST="https://${STAGE}" \
    -e LOCUST_MODE=slave \
    -e LOCUST_MASTER=locust-master \
    --rm -d grubykarol/locust:latest
    ;;
*)
    echo "USAGE: docker.sh <mqtt or https>"
    ;;
esac