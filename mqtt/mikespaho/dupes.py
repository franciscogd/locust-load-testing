import configparser
import datetime
import time

import paho.mqtt.client as mqtt
import ssl

from dateutil.tz import tzutc


class TestAgent(object):
    def __init__(self, config):
        """

        :type config: configparser.ConfigParser
        """
        super().__init__()

        client = mqtt.Client(client_id=config['DEFAULT']['client_id'], clean_session=False)
        client.username_pw_set("{}/{}".format(
            config.get('DEFAULT', 'tenant_id'),
            config.get('DEFAULT', 'username')),
            config.get('DEFAULT', 'password'))
        client.enable_logger()
        client.reconnect_delay_set(min_delay=1, max_delay=20)
        client.on_publish = self.on_publish

        tenant_url = ".".join([config.get('DEFAULT', 'tenant_id'),
                               config.get('DEFAULT', 'base_url')])

        port = config.getint('DEFAULT', 'port')
        if port == 8883:
            print("Enabling TLS")
            client.tls_set(ca_certs=None, certfile=None, keyfile=None,
                           cert_reqs=ssl.CERT_NONE,
                           tls_version=ssl.PROTOCOL_TLS, ciphers=None)

        print("Connecting to: {}".format(tenant_url))
        client.connect(tenant_url,
                       port,
                       keepalive=60)
        print("MQTT LOOP START")
        client.loop_start()

        self.client = client
        self.config = config
        self.message_ids = []

    def on_publish(self, client, userdata, mid):
        try:
            self.message_ids.remove(mid)
        except KeyError as e:
            print("missing mid {}".format(mid))
            raise

    def start(self):

        count = self.config.getint('DEFAULT', 'demo_total_measurement_count')
        print('sending messages')


        from dateutil.parser import parse
        now = parse(self.config['DEFAULT']['demo_measurements_start_time']).timestamp()

        log_interval = count / (100/10)

        for i in range(count):
            send_time = now + 0.025 * i
            date = datetime.datetime.fromtimestamp(send_time, tz=tzutc()).isoformat(timespec='milliseconds')

            if self.config.getboolean('DEFAULT', 'demo_limit_queue_size'):
                self.DEMO_WAIT_ON_PAHO_QUEUE_TO_CLEAR()

            self.message_ids.append(self.client.publish('s/uc/t004', '912,{},{}'.format(date, i), qos=1)[1])

            if i % log_interval == 0:
                print('{}% of messages queued'.format(i*100/count))

        print('queueing complete, waiting...')

        while self.get_num_messages_in_flight() > 0:
            time.sleep(1)
            print('waiting on {} messages to be sent....'.format(str(self.get_num_messages_in_flight())))

        print('all messages sent!')

        self.client.disconnect()
        self.client.loop_stop()


    def get_num_messages_in_flight(self):
        return len(self.message_ids)

    def DEMO_WAIT_ON_PAHO_QUEUE_TO_CLEAR(self):
        while self.get_num_messages_in_flight() >= self.config.getint('DEFAULT', 'demo_paho_queue_size'):
            print('paho queue full, waiting...')
            time.sleep(1)


def main():
    config = configparser.ConfigParser()

    if len(config.read('config.conf')) < 1:
        raise Exception('Failed to read config')

    agent = TestAgent(config)
    agent.start()


if __name__ == '__main__':
    main()
