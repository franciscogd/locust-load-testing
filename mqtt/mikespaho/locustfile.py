import time
import configparser
from locust import HttpLocust, TaskSet, task
from dupes import TestAgent

# this value is the number of seconds to be used before retrying operations (valid for QoS >1)
RETRY = 5

# ms
PUBLISH_TIMEOUT = 10000
SUBSCRIBE_TIMEOUT = 10000


class MyTaskSet(TaskSet):
    # @task
    # def pubqos0(self):
    #     topic = os.getenv('MQTT_TOPIC', '')
    #     if topic == '':
    #       raise ValueError("Please set environment variable MQTT_TOPIC")
    #     self.client.publish(topic, payload=self.payload(), qos=0, name='publish:qos0:'+topic, timeout=PUBLISH_TIMEOUT)

    def on_start(self):
        # allow for the connection to be established before doing anything (publishing or subscribing) to the MQTT topic
        time.sleep(1)

        try:
            config = configparser.ConfigParser()
            if len(config.read('mikespaho.config.conf')) < 1:
                raise Exception('Failed to read config')
        except Exception:
            raise RuntimeError('Failed to read config')

        agent = TestAgent(config)
        agent.start()


    # def payload(self):
    #     payload = {
    #        'temperature': random.randrange(0,10,1)  # set temperature between 0 and 10
    #        }
    #     return json.dumps(payload)


"""
   Locust hatches several instances of this class, according to the number of simulated users
   that we define in the GUI. Each instance of MyThing represents a device.
"""


class MyLocust(TestAgent):
    task_set = MyTaskSet
    min_wait = 1000
    max_wait = 1500
