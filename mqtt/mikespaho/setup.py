from setuptools import setup, find_packages

setup(
    name='unified_edge',
    version='0.1.0',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'paho-mqtt==1.4.0',
        'python-dateutil==2.8.0',
    ],
    entry_points='''
        [console_scripts]
        testcase=demo.dupes_test_case:main
    ''',
)
