import time
import ssl
import configparser
import paho.mqtt.client as mqtt

cur_client_id = 123


def time_delta(t1, t2):
    return int((t2 - t1) * 1000)


class LocustError(Exception):
    pass


class TimeoutError(ValueError):
    pass


class DisconnectError(Exception):
    pass


class Message(object):
    def __init__(self, topic, payload, start_time, timeout, name):
        self.topic = topic
        self.payload = payload
        self.start_time = start_time
        self.timeout = timeout
        self.name = name

    def timed_out(self, total_time):
        return self.timeout is not None and total_time > self.timeout


class MQTTClient(mqtt.Client):

    def __init__(self, config, on_success, on_failure, *args, **kwargs):
        # super(MQTTClient, self).__init__(*args, **kwargs)
        self.config = config
        super().__init__(*args, **kwargs)

        self.username_pw_set("{}/{}".format(self.config.get('DEFAULT', 'tenant_id'),
                                            self.config.get('DEFAULT', 'username')),
                             config.get('DEFAULT', 'password'))
        self.enable_logger()
        self.reconnect_delay_set(min_delay=1, max_delay=20)

        self.tenant_url = ".".join([config.get('DEFAULT', 'tenant_id'),
                               config.get('DEFAULT', 'base_url')])

        self.port = config.getint('DEFAULT', 'port')
        if self.port == 8883:
            print("Enabling TLS")
            self.tls_set(ca_certs=None, certfile=None, keyfile=None,
                         cert_reqs=ssl.CERT_NONE, tls_version=ssl.PROTOCOL_TLS, ciphers=None)

        self.on_success = on_success
        self.on_failure = on_failure

        self.mmap = {}

    def __lt__(self, fuku):
        return False

    def __gt__(self, fuk_u):
        return False

    def publish(self, topic, payload=None, repeat=1, name='mqtt', **kwargs):
        self.check_for_locust_timeouts(time.time())

        timeout = kwargs.pop('timeout', 5)
        for i in range(repeat):
            start_time = time.time()
            try:
                err, mid = super(MQTTClient, self).publish(
                    topic,
                    payload=payload,
                    **kwargs
                )
                if err:
                    raise ValueError(err)
                self.mmap[mid] = Message(
                        topic, payload, start_time, timeout, name
                        )
            except Exception as e:
                total_time = time.time() - start_time
                self.on_failure(
                    request_type='mqtt',
                    name=name,
                    response_time=total_time,
                    exception=e,
                )

    def on_publish(self, client, userdata, mid):
        end_time = time.time()
        message = self.mmap.pop(mid, None)
        if message is None:
            return
        total_time = end_time - message.start_time
        if message.timed_out(total_time):
            self.on_failure(
                request_type='mqtt',
                name=message.name,
                response_time=total_time,
                exception=TimeoutError("publish timed out"),
            )
        else:
            self.on_success(
                request_type='mqtt',
                name=message.name,
                response_time=total_time,
                response_length=len(message.payload),
            )
        self.check_for_locust_timeouts(end_time)

    def on_disconnect(self, client, userdata, rc):
        self.on_failure(
            request_type='mqtt',
            name='client',
            response_time=0,
            exception=DisconnectError("disconnected"),
        )

    def on_connect(self, client, userdata, flags, rc):
        print(" Connected flags " + str(flags) + " result code " \
                     + str(rc) + " client_id "+ str(cur_client_id))
        client.connected_flag = True

    def check_for_locust_timeouts(self, end_time):
        timed_out = [mid for mid, msg in dict(self.mmap).items()
                     if msg.timed_out(end_time - msg.start_time)]
        for mid in timed_out:
            msg = self.mmap.pop(mid)
            total_time = end_time - msg.start_time
            self.on_failure(
                request_type='mqtt',
                name=msg.name,
                response_time=total_time,
                exception=TimeoutError(
                    "message not received in {}s".format(msg.timeout)
                    ),
            )


class MQTTLocust(object):

    def __init__(self, on_success, on_failure):
        super().__init__()

        config = configparser.ConfigParser()
        self.config = config
        self.message_ids = []

        if len(config.read('config.conf')) < 1:
            raise Exception('Failed to read config')

        global cur_client_id
        client_id = cur_client_id
        cur_client_id = cur_client_id + 1
        self.client = MQTTClient(config, on_success, on_failure, client_id=str(client_id))
        host = self.client.tenant_url
        port = self.client.port

        self.client.connect(host, port=port)
        self.client.loop_start()

    # def start(self):
    #     from dateutil.parser import parse
    #
    #     now = parse(self.config['DEFAULT']['demo_measurements_start_time']).timestamp()
    #     date = datetime.datetime.fromtimestamp(now, tz=tzutc()).isoformat(timespec='milliseconds')
    #
    #     self.client.publish('s/uc/t004', '912,{},{}'.format(date, 498), qos=1)

    def get_num_messages_in_flight(self):
        return len(self.message_ids)

    def DEMO_WAIT_ON_PAHO_QUEUE_TO_CLEAR(self):
        while self.get_num_messages_in_flight() >= self.config.getint('DEFAULT', 'demo_paho_queue_size'):
            print('paho queue full, waiting...')
            time.sleep(1)


def main():
    mqtt_locust = MQTTLocust(lambda **kwargs: print(kwargs), lambda **kwargs: print(kwargs))
    # mqtt_locust.start()
    while True:
        time.sleep(.25)


if __name__ == '__main__':
    main()
