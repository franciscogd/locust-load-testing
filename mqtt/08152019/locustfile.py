import random
import datetime
# import resource
# resource.setrlimit(resource.RLIMIT_NOFILE, (999999, 999999))
import sys
from dateutil.tz import tzutc
from locust import TaskSet, task, events, Locust
from mqtt_locust import MQTTLocust

sys.setrecursionlimit(3000)
TIMEOUT = 60
REPEAT = 100
MQTT_5K_DAT_FILE = 'data/bulk_message_65-5k.dat'  # ~ 5k (4,998 bytes)
MQTT_8k_DAT_FILE = 'data/bulk_message_65-8k.dat'  # ~ 8k (7,997 bytes)


class MyTaskSet(TaskSet):
    @task(1)
    def qos0(self):
        print('publish')
        self.client.client.publish(
            's/uc/t004/4321', self.payload(MQTT_5K_DAT_FILE), qos=0, timeout=TIMEOUT,
            # 's/uc/t004/4321', self.payload(), qos=0,
            repeat=REPEAT, name='qos0 publish'
        )
        print('qos0 publish done')

    # @task(1)
    # def qos1(self):
    #     print('publish')
    #     self.client.client.publish(
    #         's/uc/t004/4321', self.payload(), qos=1, timeout=TIMEOUT,
    #         # 's/uc/t004/4321', self.payload(), qos=1,
    #         repeat=REPEAT, name='qos1 publish'
    #     )
    #     print('qos1 publish done')
    #
    # @task(1)
    # def qos2(self):
    #     print('publish')
    #     self.client.client.publish(
    #         's/uc/t004/4321', self.payload(), qos=2, timeout=TIMEOUT,
    #         # 's/uc/t004/4321', self.payload(), qos=2,
    #         repeat=REPEAT, name='qos2 publish'
    #     )
    #     print('qos2 publish done')

    # def payload(self):
    #     now = datetime.datetime.now().timestamp()
    #     date = datetime.datetime.fromtimestamp(now, tz=tzutc()).isoformat(timespec='milliseconds')
    #
    #     return '912,{},{}'.format(date, random.randrange(0, 100, 10))

    @staticmethod
    def payload(dat_file):
        with open(dat_file, 'r') as file:
            mqtt_dat = file.read()
            print(mqtt_dat)

        return mqtt_dat


def fire_locust_failure(**kwargs):
    events.request_failure.fire(**kwargs)


def fire_locust_success(**kwargs):
    events.request_success.fire(**kwargs)


class MyLocust(Locust):
    task_set = MyTaskSet
    task_num = len(task_set.tasks)
    print('task_num: {}'.format(task_num))
    tasks_cycle_time = 1  # time in s per cycle of requests

    # minimum and maximum time in ms that a simulated user will wait between executing each task.
    min_wait = (tasks_cycle_time // task_num) * 1000  # in ms
    print('min_wait: {}'.format(min_wait))
    max_wait = (tasks_cycle_time // task_num) * 1000 + 1000  # in ms
    print('max_wait: {}'.format(max_wait))
    # min_wait = 5000
    # max_wait = 15000

    def __init__(self):
        super().__init__()
        self.client = MQTTLocust(fire_locust_success, fire_locust_failure)
