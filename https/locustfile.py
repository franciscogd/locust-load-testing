import random

from locust import HttpLocust, TaskSet, task
import os


class MyTaskSet(TaskSet):

    """ on_start is called when a Locust start before any task is scheduled """
    def on_start(self):

        try:
            basic_auth = os.environ["C8Y_API_BASIC_AUTH"]
            self.headers = {"Authorization": "Basic " + str(basic_auth),
                            "UseXBasic": "true",
                            "Connection": "keep-alive"}
        except Exception:
            raise RuntimeError("The C8Y_API_BASIC_AUTH environment variable needs to be set.")

        # with self.client.post('/login', headers=self.headers) as response:
        #     print('POST: self.headers: {}'.format(self.headers))
        #     print('POST: response.status_code: {}'.format(response.status_code))
        #     if response.status_code == 401:
        #         response.failure("401 Unauthorized: Invalid credentials!")
        res = self.client.post('/login', headers=self.headers)
        print('POST: self.headers: {}'.format(self.headers))
        print('POST: res.status_code: {}'.format(res.status_code))
        res.raise_for_status()

    @task(20)
    def is_device_page_size2000(self):
        with self.client.get("/inventory/managedObjects?fragmentType=c8y_IsDevice&pageSize=2000",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: self.headers: {}'.format(self.headers))
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("401 Unauthorized: Invalid credentials!")

    @task(20)
    def is_device_page_size1000(self):
        with self.client.get("/inventory/managedObjects?fragmentType=c8y_IsDevice&pageSize=1000",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: self.headers: {}'.format(self.headers))
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("401 Unauthorized: Invalid credentials!")

    @task(20)
    def is_device_page_size500(self):
        with self.client.get("/inventory/managedObjects?fragmentType=c8y_IsDevice&pageSize=500",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: self.headers: {}'.format(self.headers))
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("401 Unauthorized: Invalid credentials!")

    @task(20)
    def is_device_page_size70(self):
        with self.client.get("/inventory/managedObjects?fragmentType=c8y_IsDevice&pageSize=70",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: self.headers: {}'.format(self.headers))
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("401 Unauthorized: Invalid credentials!")

    @task(20)
    def is_device_ids(self):
        with self.client.get("/inventory/managedObjects?fragmentType=c8y_IsDevice&ids=67279,22865412,4180922,2359982,"
                             "507,29297294,24511757,28029118,33329627,29206132,29206134,33742773,24675059,33781845,"
                             "29296851,33313378,28211675,50436594,5074180922,24511757,24773630,28143920,28211675,"
                             "33329627,28029118,29297294,24675059,29206132,29296851,33313378,33781845,42166601,"
                             "29206134,33742773,33012727,34729269,35905593,28816600,50436594,68718738,69637330",
                             headers=self.headers, catch_response=True) as response:
            print('POST: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("401 Unauthorized: Invalid credentials!")


class MyLocust(HttpLocust):
    task_set = MyTaskSet
    min_wait = 1000
    max_wait = 2000