import random

from locust import HttpLocust, TaskSet, task
import os


# def task_factory(path):
#     def _locust(locust):
#         # print('locust.client.headers: {}'.format(str(locust.client.headers)))
#         with locust.client.get(path, headers=locust.client.headers, catch_response=True) as response:
#             print('GET: response.status_code: {}'.format(response.status_code))
#             if response.status_code == 401:
#                 response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))
#
#     return _locust


class MyTaskSet(TaskSet):
    """ on_start is called when a Locust start before any task is scheduled """

    def on_start(self):

        try:
            self.tenant = os.environ["TENANT"]
            self.device_id = os.environ["DEVICE_ID"]
            self.headers = {"Authorization": "Basic " + os.environ["C8Y_API_BASIC_AUTH"],
                            "UseXBasic": "true",
                            "Connection": "keep-alive"}

        except Exception:
            raise RuntimeError("The proper environment variables need to be set.")

        with self.client.post('/login', headers=self.headers) as response:
            print('POST: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_1(self):
        with self.client.get("/measurement/measurements?source={}&pageSize=2000&revert=false"
                             "&dateFrom=2019-03-15T23:02:59.599Z&dateTo=2019-04-15T23:02:59.599Z"
                             "&pageSize=2000".format(self.tenant),
                             headers=self.headers, catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_2(self):
        with self.client.get("/inventory/managedObjects/{}".format(self.device_id),
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_3(self):
        with self.client.get("/measurement/measurements?source={}&pageSize=2000&revert=false"
                             "&dateFrom=2019-03-15T23:02:59.599Z&dateTo=2019-04-15T23:02:59.599Z"
                             "&pageSize=2000".format(self.device_id),
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_4(self):
        with self.client.get("/user/{}/users?pageSize=100".format(self.tenant),
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_5(self):
        with self.client.get("/inventory/managedObjects?pageSize=100&query=$filter%3D((type+eq+%27c8y_DeviceGroup%27)"
                             "+or+(type+eq+%27c8y_DynamicGroup%27))&withTotalPages=true",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_6(self):
        with self.client.get("/inventory/managedObjects?fragmentType=c8y_IsDevice&pageSize=1&skipChildrenNames=true"
                             "&withTotalPages=true",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_7(self):
        with self.client.get("/inventory/managedObjects?pageSize=100&q=&withGroups=true&withTotalPages=true",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_8(self):
        with self.client.get("/user/{}/users?currentPage=2&pageSize=100&withSubusersCount=true"
                             "&withTotalPages=true".format(self.tenant),
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_9(self):
        with self.client.get("/tenant/statistics/summary?dateFrom=2019-06-30&dateTill=2019-07-31T23:59:59-04:00",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_10(self):
        with self.client.get("/application/applicationsByTenant/infrastructure?pageSize=100&withTotalPages=true",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_11(self):
        with self.client.get("/user/{}/users?pageSize=1&withTotalPages=true".format(self.tenant_id),
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_12(self):
        with self.client.get("/inventory/managedObjects?fragmentType=c8y_Report&pageSize=1000&withTotalPages=true",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_13(self):
        with self.client.get("/inventory/managedObjects?fragmentType=c8y_Dashboard!name!home-cockpit1&pageSize=100"
                             "&withTotalPages=true",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_14(self):
        with self.client.get("/inventory/managedObjects?fragmentType=c8y_IsDevice&pageSize=1&skipChildrenNames=true"
                             "&withTotalPages=true",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_15(self):
        with self.client.get("/inventory/managedObjects?fragmentType=languagessen&pageSize=100&withTotalPages=true",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_16(self):
        with self.client.get("/inventory/managedObjects?fragmentType=c8y_hideWelcomeScreenssen&pageSize=100"
                             "&withTotalPages=true",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_17(self):
        with self.client.get("/application/applicationsByUser/ssen?dropOverwrittenApps=true&noPaging=true"
                             "&pageSize=100&withTotalPages=true",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_18(self):
        with self.client.get("/inventory/managedObjects?fragmentType=unitssen",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_19(self):
        with self.client.get("/inventory/managedObjects?fragmentType=passwordExpirationAlertsDisplayedssen"
                             "&pageSize=100&withTotalPages=true",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_20(self):
        with self.client.get("/application/applicationsByUser/ssen?dropOverwrittenApps=false&noPaging=true"
                             "&pageSize=100&withTotalPages=true",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_21(self):
        with self.client.get("/alarm/alarms/count?resolved=false&severity=CRITICAL",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_22(self):
        with self.client.get("/alarm/alarms/count?resolved=false&severity=MAJOR",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_23(self):
        with self.client.get("/alarm/alarms/count?resolved=false&severity=MINOR",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_24(self):
        with self.client.get("/alarm/alarms?currentPage=2&dateFrom=1970-01-01&dateTo=2019-08-14T11:09:11-04:00"
                             "&pageSize=100&resolved=false&severity=CRITICAL",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_25(self):
        with self.client.get("/alarm/alarms?currentPage=2&dateFrom=1970-01-01&dateTo=2019-08-14T11:09:11-04:00&"
                             "pageSize=100&resolved=false&severity=MAJOR",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    @task(1)
    def task_26(self):
        with self.client.get("/alarm/alarms?currentPage=2&dateFrom=1970-01-01&dateTo=2019-08-14T11:09:11-04:00"
                             "&pageSize=100&resolved=false&severity=MINOR",
                             headers=self.headers,
                             catch_response=True) as response:
            print('GET: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))


class MyLocust(HttpLocust):
    task_set = MyTaskSet
    task_num = len(task_set.tasks)
    print('task_num: {}'.format(task_num))
    tasks_cycle_time = 30  # time in s per cycle of requests

    # minimum and maximum time in ms that a simulated user will wait between executing each task.
    min_wait = (tasks_cycle_time // task_num) * 1000  # in ms
    print('min_wait: {}'.format(min_wait))
    max_wait = (tasks_cycle_time // task_num) * 1000 + 1000  # in ms
    print('max_wait: {}'.format(max_wait))
