from locust import HttpLocust, TaskSet, task
import os


# def task_factory(path):
#     def _task(locust):
#         print('locust.client.headers: {}'.format(str(locust.client.headers)))
#         with locust.client.get(path, headers=locust.client.headers, catch_response=True) as response:
#             print('GET: response.status_code: {}'.format(response.status_code))
#             if response.status_code == 401:
#                 response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))
#
#     return _task


class MyTaskSet(TaskSet):
    """ on_start is called when a Locust start before any task is scheduled """

    def on_start(self):

        try:
            self.tenant = os.environ["TENANT"]
            self.device_id = os.environ["DEVICE_ID"]
            self.headers = {"Authorization": "Basic " + os.environ["C8Y_API_BASIC_AUTH"],
                            "UseXBasic": "true",
                            "Connection": "keep-alive"}

        except Exception:
            raise RuntimeError("The proper environment variables need to be set.")

        with self.client.post('/login', headers=self.headers) as response:
            print('POST: response.status_code: {}'.format(response.status_code))
            if response.status_code == 401:
                response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))

    def task_factory(self, path):
        def _task():
            print('locust.client.headers: {}'.format(str(self.headers)))
            with self.client.get(path, headers=self.headers, catch_response=True) as response:
                print('GET: response.status_code: {}'.format(response.status_code))
                if response.status_code == 401:
                    response.failure("{} Unauthorized: Invalid credentials!".format(response.status_code))
        return _task

    def create_tasks(self):
        _tasks = {self.task_factory(path): 1 for path in [
            "/measurement/measurements?source={}&pageSize=2000&revert=false"
            "&dateFrom=2019-03-15T23:02:59.599Z&dateTo=2019-04-15T23:02:59.599Z"
            "&pageSize=2000".format(self.device_id),
            "/inventory/managedObjects/{}".format(self.device_id),
            "/inventory/managedObjects?fragmentType=c8y_IsDeviceGroup&pageSize=2000",
            "/user/{}/users?pageSize=100".format(self.tenant),
            "/inventory/managedObjects?pageSize=100&query=$filter%3D((type+eq+%27c8y_DeviceGroup%27)"
            "+or+(type+eq+%27c8y_DynamicGroup%27))&withTotalPages=true",
            "/inventory/managedObjects?fragmentType=c8y_IsDevice&pageSize=1&skipChildrenNames=true"
            "&withTotalPages=true",
            "/inventory/managedObjects?pageSize=100&q=&withGroups=true&withTotalPages=true",
            "/user/{}/users?currentPage=2&pageSize=100&withSubusersCount=true"
            "&withTotalPages=true".format(self.tenant),
            "/tenant/statistics/summary?dateFrom=2019-06-30&dateTill=2019-07-31T23:59:59-04:00",
            "/application/applicationsByTenant/{}?pageSize=100&withTotalPages=true".format(self.tenant),
            "/user/{}/users?pageSize=1&withTotalPages=true".format(self.tenant),
            "/inventory/managedObjects?fragmentType=c8y_Report&pageSize=1000&withTotalPages=true",
            "/inventory/managedObjects?fragmentType=c8y_Dashboard!name!home-cockpit1&pageSize=100"
            "&withTotalPages=true",
            "/inventory/managedObjects?fragmentType=c8y_IsDevice&pageSize=1&skipChildrenNames=true"
            "&withTotalPages=true",
            "/inventory/managedObjects?fragmentType=languagessen&pageSize=100&withTotalPages=true",
            "/inventory/managedObjects?fragmentType=c8y_hideWelcomeScreenssen&pageSize=100"
            "&withTotalPages=true",
            "/application/applicationsByUser/ssen?dropOverwrittenApps=true&noPaging=true"
            "&pageSize=100&withTotalPages=true",
            "/inventory/managedObjects?fragmentType=unitssen",
            "/inventory/managedObjects?fragmentType=passwordExpirationAlertsDisplayedssen"
            "&pageSize=100&withTotalPages=true",
            "/application/applicationsByUser/ssen?dropOverwrittenApps=false&noPaging=true&pageSize=100"
            "&withTotalPages=true",
            "/alarm/alarms/count?resolved=false&severity=CRITICAL",
            "/alarm/alarms/count?resolved=false&severity=MAJOR",
            "/alarm/alarms/count?resolved=false&severity=MINOR",
            "/alarm/alarms?currentPage=2&dateFrom=1970-01-01&dateTo=2019-08-14T11:09:11-04:00&pageSize=100"
            "&resolved=false&severity=CRITICAL",
            "/alarm/alarms?currentPage=2&dateFrom=1970-01-01&dateTo=2019-08-14T11:09:11-04:00&pageSize=100"
            "&resolved=false&severity=MAJOR",
            "/alarm/alarms?currentPage=2&dateFrom=1970-01-01&dateTo=2019-08-14T11:09:11-04:00&pageSize=100"
            "&resolved=false&severity=MINOR",
        ]}
        return _tasks

    tasks = create_tasks()
    # @task
    # def task_1(self):
    #     pass


class MyLocust(HttpLocust):
    task_set = MyTaskSet

    # task_num = len(task_set.tasks)
    # print('task_num: {}'.format(task_num))
    # tasks_cycle_time = 30  # time in s per cycle of requests
    #
    # # minimum and maximum time in ms that a simulated user will wait between executing each task.
    # min_wait = (tasks_cycle_time // task_num) * 1000  # in ms
    # print('min_wait: {}'.format(min_wait))
    # max_wait = (tasks_cycle_time // task_num) * 1000 + 1000  # in ms
    # print('max_wait: {}'.format(max_wait))

    min_wait = 1000
    max_wait = 2000
