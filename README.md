# Locust Tests

## Locust
Locust is an easy-to-use, distributed, user load testing tool. It is intended for load-testing web sites (or other systems) and figuring out how many concurrent users a system can handle.

## Web UI
Locust provides a small HTTP API and a Web UI to control the execution of the tests and browsing through the results. One can use the API, for example, to automatically trigger stress tests as a part of build process.

## Locust Documentation
https://docs.locust.io/en/latest/what-is-locust.html

## Locust Files
Locust files are found in the `mqtt` and `https` directories.

## Dependencies
* Locust supports Python 2.7, 3.4, 3.5, and 3.6
* Locust (`pip install locustio==0.8.1`)
* Paho MQTT client (`pip install paho-mqtt==1.4.0`)
* Python dateutil (`pip install python-dateutil==2.8.0`)

## mqtt/.../mqtt_locust.py
This file contains logic to create an MQTT client, initiate a connection, publish messages to a topic and subscribe to
a topic. It also reports failure or success results to Locust. A subscribing client can set the maximum quality of service
a server uses to send messages that match the client subscriptions. 
The MQTTClient_subscribe() and MQTTClient_subscribeMany() functions set this maximum. The QoS of a message forwarded to a 
subscriber thus might be different to the QoS given to the message by the original publisher.
The lower of the two values is used to forward a message.

The three levels are:
* QoS0, At most once: The message is delivered at most once, or it may not be delivered at all. Its delivery across the
network is not acknowledged. The message is not stored. The message could be lost if the client is disconnected, or if
the server fails. QoS0 is the fastest mode of transfer. It is sometimes called "fire and forget". The MQTT protocol does 
not require servers to forward publications at QoS0 to a client. If the client is disconnected at the time the server 
receives the publication, the publication might be discarded, depending on the server implementation.

* QoS1, At least once: The message is always delivered at least once. It might be delivered multiple times if there is
a failure before an acknowledgment is received by the sender. The message must be stored locally at the sender, until
the sender receives confirmation that the message has been published by the receiver. The message is stored in case the
message must be sent again.

* QoS2, Exactly once: The message is always delivered exactly once. The message must be stored locally at the sender,
until the sender receives confirmation that the message has been published by the receiver. The message is stored in
case the message must be sent again. QoS2 is the safest, but slowest mode of transfer. A more sophisticated handshaking
and acknowledgement sequence is used than for QoS1 to ensure no duplication of messages occurs.

Another important feature is the support for TLSv1.2, which is not required by Cumulocity yet, but it will be needed in
order to establish a secure connection.

## mqtt-locust/.../locustfile.py
The _locustfile.py_ is a normal Python file that declares at least one class that inherits from the class `Locust`.
This file creates a basic locust test that publishes messages to an IoT topic. The payload is a random value
between 0 and 10, which represents a temperature reading.

## Certificates
Set environment variables CA_CERT, IOT_CERT and IOT_PRIVATE_KEY with the full location of your .pem files.

## Topics
Set the environment variable MQTT_TOPIC with the topic you want to publish messages to.

## Running the Test
First you need to get the endpoint. For example, if using AWS IoT, you can use the AWS CLI: `aws iot describe-endpoint`.
Then run the locust command from your environment:

```bash
export CA_CERT="$HOME/.ssh/ca_iot_cert.pem"
export IOT_CERT="$HOME/.ssh/iot_cert.pem"
export IOT_PRIVATE_KEY="$HOME/.ssh/iot.pem"
export MQTT_TOPIC="s/us"
locust --host="uiotsandbox.stage.sbdconnect.io" -f locusftile.py
```

## Locust

### `Locust` class
A `Locust` class represents one user (or a swarming locust if you will). **Locust** will spawn (hatch) one instance of
the `Locust` class for each user that is being simulated. There are a few attributes that a `Locust` class should
typically define.

Here we define a number of **Locust** tasks, which are normal Python callables that take one argument (a `Locust` class
instance). **These tasks are gathered under a `TaskSet` class in the _tasks_ attribute**. Then we have an `HttpLocust`
class which represents a user, where we define how long a simulated user should wait between executing tasks, as well
as what `TaskSet` class should define the user’s “behaviour”. `TaskSet` classes can be nested.

```python
from locust import HttpLocust, TaskSet


def login(l):
    l.client.post("/login", {"username":"fduran", "password":"password"})


def logout(l):
    l.client.post("/logout", {"username":"fduran", "password":"password"})


def index(l):
    l.client.get("/")


def profile(l):
    l.client.get("/profile")


class UserBehavior(TaskSet):
    tasks = {index: 2, profile: 1}

    def on_start(self):
        login(self)

    def on_stop(self):
        logout(self)


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 9000
```

The `HttpLocust` class inherits from the `Locust` class, and it adds a _client_ attribute which is an instance of
`HttpSession` that can be used to make HTTP requests.

The min/max wait times control the amount of time each simulated user waits between executing tasks. Each user will 
execute a task at random, wait a random amount of time between `min_wait` and `max_wait`, and then repeat.

### The _task_set_ attribute

The _task_set_ attribute should point to a `TaskSet` class which defines the behaviour of the user.

### The _min_wait_ and _max_wait_ attributes
In addition to the _task_set_ attribute, one usually wants to declare the _min_wait_ and _max_wait_ attributes.
These are the minimum and maximum times respectively, in milliseconds, that a simulated user will wait between
executing each task. _min_wait_ and _min_wait_ default to 1000, and therefore a locust will always wait 1 second
between each task if _min_wait_ and _min_wait_ are not declared.


### The _weight_ attribute
You can run two locusts from the same file like so:

```bash
$ locust -f locust_file.py WebUserLocust MobileUserLocust
```

If you wish to make one of these locusts execute more often you can set a weight attribute on those classes.
Say for example, web users are three times more likely than mobile users:

```python
class WebUserLocust(Locust):
    weight = 3
    ...

class MobileUserLocust(Locust):
    weight = 1
    ...
```

###  The host attribute
The _host_ attribute is a URL prefix (i.e. “https://google.com”) to the host that is to be loaded.
Usually, this is specified on the command line, using the `--host` option, when locust is started.
If one declares a _host_ attribute in the `Locust` class, it will be used in the case when no `--host` is specified on
the command line.

### `TaskSet` class
A TaskSet is a collection of tasks. If we were load-testing an auction website, the tasks would be
“loading the start page”, “searching for some product”, and “making a bid”.
When a load test is started, each instance of the spawned `Locust` classes will start executing their `TaskSet`. What
happens then is that each `TaskSet` will pick one of its tasks and call it. It will then wait a number of milliseconds.
Then it will again pick a new task to be called, wait again, and so on.
Here is an example:

```python
from locust import Locust, TaskSet, task

class MyTaskSet(TaskSet):
    @task
    def my_task(self):
        print("Locust instance (%r) executing my_task" % (self.locust))

class MyLocust(Locust):
    task_set = MyTaskSet
```

### Declaring tasks
The typical way of declaring tasks for a `TaskSet` class it to use the `@task` decorator. The following code is
equivalent to the one shown in the **`Locust` class** section:

```python
from locust import HttpLocust, TaskSet, task

class UserBehavior(TaskSet):
    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        self.login()

    def on_stop(self):
        """ on_stop is called when the TaskSet is stopping """
        self.logout()

    def login(self):
        self.client.post("/login", {"username":"fduran", "password":"password"})

    def logout(self):
        self.client.post("/logout", {"username":"fduran", "password":"password"})

    @task(2)
    def index(self):
        self.client.get("/")

    @task(1)
    def profile(self):
        self.client.get("/profile")

class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 9000
```

`@task` takes an optional weight argument that can be used to specify the task’s execution ratio.
In the following example, _task2_ will be executed twice as much as _task1_:

```python
from locust import Locust, TaskSet, task

class MyTaskSet(TaskSet):
    min_wait = 5000
    max_wait = 15000

    @task(3)
    def task1(self):
        pass

    @task(6)
    def task2(self):
        pass

class MyLocust(Locust):
    task_set = MyTaskSet
```


### _tasks_ attribute
Using the `@task` decorator to declare tasks is a convenience, and usually the best way to do it. However, it’s also
possible to define the tasks of a `TaskSet` by setting the _tasks_ attribute (using the @task decorator will actually
just populate the _tasks_ attribute).

The _tasks_ attribute is either a list of Python callables, or a <callable : int> dict. The _tasks_ are Python
callables that receive one argument —the `TaskSet` class instance that is executing the task. Here is an
example:

```python
from locust import Locust, TaskSet

def my_task(l):
    pass

class MyTaskSet(TaskSet):
    tasks = [my_task]

class MyLocust(Locust):
    task_set = MyTaskSet
```
If the _tasks_ attribute is specified as a list, each time a task is to be performed, it will be randomly chosen from
the _tasks_ attribute. If however, _tasks_ is a dict —with callables as keys and ints as values— the task that is to be
executed will be chosen at random but with the int as ratio. So with a tasks that looks like this:

```python
{my_task: 3, another_task: 1}
```

### `TaskSet` classes can be nested
A very important property of `TaskSet` classes is that they can be nested. Nesting `TaskSet` classes will allow us to
define a behaviour that simulates users in a more realistic way. For example we could define `TaskSet` classes with the
following structure:

    Main user behaviour

        Index page
        Forum page

            Read thread

                Reply

            New thread
            View next page

        Browse categories

            Watch movie
            Filter movies

        About page

The way you nest `TaskSet` classes just like when you specify a task using the _tasks_ attribute, but instead of
referring to a Python function, you refer to another `TaskSet` class:

```python
class ForumPage(TaskSet):
    @task(20)
    def read_thread(self):
        pass

    @task(1)
    def new_thread(self):
        pass

    @task(5)
    def stop(self):
        self.interrupt()

class UserBehaviour(TaskSet):
    tasks = {ForumPage:10}

    @task
    def index(self):
        pass
```

In the above example, if the `ForumPage` would get selected for execution when the `UserBehaviour` `TaskSet` is
executing, then the `ForumPage` `TaskSet` would start executing. The `ForumPage` `TaskSet` would then pick one of its
own tasks, execute it, wait, and so on.

Note the _self.interrupt()_ in the `ForumPage` stop method. What this does is essentially to stop executing the
`ForumPage` task set and the execution will continue in the `UserBehaviour` instance. If we didn't have a call to the
_interrupt()_ method somewhere in `ForumPage`, the `Locust` would never stop running the `ForumPage` task once it has
started. But by having the interrupt function, we can—together with task weighting—define how likely it is that a
simulated user leaves the forum.

It’s also possible to declare a nested `TaskSet`, inline in a class, using the `@task` decorator, just like when
declaring normal tasks:

```python
class MyTaskSet(TaskSet):
    @task
    class SubTaskSet(TaskSet):
        @task
        def my_task(self):
            pass
```

### `TaskSequence` class
`TaskSequence` is a `TaskSet` but its tasks will be executed in order.
```python
class MyTaskSequence(TaskSequence):
    @seq_task(1)
    def first_task(self):
        pass

    @seq_task(2)
    def second_task(self):
        pass

    @seq_task(3)
    @task(10)
    def third_task(self):
        pass
```
In the above example, the order is defined to execute first_task, then second_task and lastly the third_task for 10
times. As you can see, you can compose  `@seq_task` with `@task` decorator, and of course you can also nest `TaskSet`
classes within `TaskSequence` classes and vice versa.


### _Setups_, _Teardowns_, _on_start_, and _on_stop_
Locust optionally supports `Locust` level methods _setup_ and _teardown_, `TaskSet` level _setup_, _teardown_,
_on_start_, and _on_stop_.

#### Setups and Teardowns

_setup_ and _teardown_, whether it’s run on `Locust` or `TaskSet`, are methods that are run only once. _setup_ is run
before tasks start running, while _teardown_ is run after all tasks have finished and Locust is exiting.
This enables you to perform some preparation before tasks start running (like creating a database) and to clean up
before Locust quits (like deleting the database).

To use, simply declare a _setup_ and/or _teardown_ on the `Locust` or `TaskSet` class.
These methods will be run for you.

#### _on_start_ and _on_stop_ methods

A `TaskSet` class can declare _on_start_ and _on_stop_ methods. _on_start_ method is called when a simulated user
starts executing that `TaskSet` class, while the _on_stop <locust.core.TaskSet.on_stop()_ method is called when the
`TaskSet` is stopped.

#### Order of Events
Since many setup and cleanup operations are dependent on each other, here is the order in which they are run:

1. Locust setup
1. TaskSet setup
1. TaskSet on_start
1. TaskSet tasks
1. TaskSet on_stop
1. TaskSet teardown
1. Locust teardown

## Making HTTP Requests
In order to actually load test a system we need to make HTTP requests. To help us do this, the `HttpLocust` class
exists. When using this class, each instance gets a _client_ attribute which will be an instance of `HttpSession` which
can be used to make HTTP requests.

The class `HttpLocust` represents an HTTP "user" which is to be hatched and attack the system that is to be load tested.

The behaviour of this user is defined by the _task_set_ attribute, which should point to a `TaskSet` class. This class
creates a client attribute on instantiation which is an HTTP client with support for keeping a user session between
requests.

### `client=None`

Instance of `HttpSession` that is created upon instantiation of Locust. The client supports cookies, and therefore
keeps the session between HTTP requests.

When inheriting from the `HttpLocust` class, we can use its client attribute to make HTTP requests against the server.
Here is an example of a _locustfile_ that can be used to load test a site with two URLs, "**/**" and "**/about/**":

```python
from locust import HttpLocust, TaskSet, task

class MyTaskSet(TaskSet):
    @task(2)
    def index(self):
        self.client.get("/")

    @task(1)
    def about(self):
        self.client.get("/about/")

class MyLocust(HttpLocust):
    task_set = MyTaskSet
    min_wait = 5000
    max_wait = 15000
```

Using the above `Locust` class, each simulated user will wait between 5 and 15 seconds between the requests, and "/"
will be requested twice as much as "/about/".

The attentive reader will find it odd that we can reference the `HttpSession` instance using _self.client_ inside the
`TaskSet`, and not _self.locust.client_. We can do this because the `TaskSet` class has a convenience property called
_client_ that simply returns _self.locust.client_.

### Using the HTTP client
Each instance of `HttpLocust` has an instance of `HttpSession` in the _client_ attribute. The `HttpSessio`n class is
actually a subclass of `requests.Session` and can be used to make HTTP requests, that will be reported to **Locust**’s
statistics, using the _get_, _post_, _put_, _delete_, _head_, _patch_ and _options_ methods. The `HttpSession` instance
will preserve cookies between requests so that it can be used to log in to websites and keep a session between requests.
The client attribute can also be referenced from the `Locust` instance’s `TaskSet` instances so that it’s easy to
retrieve the client and make HTTP requests from within your tasks.

Here’s a simple example that makes a GET request to the "/about" path (in this case we assume self is an instance of a
`TaskSet` or `HttpLocust` class:

```python
response = self.client.get("/about")
print("Response status code:", response.status_code)
print("Response content:", response.text)
```

And here’s an example making a POST request:

```python
response = self.client.post("/login", {"username":"testuser", "password":"secret"})
```

### Safe mode
The HTTP client is configured to run in safe_mode. What this does is that any request that fails due to a connection
error, timeout, or similar will not raise an exception, but rather return an empty dummy Response object.
The request will be reported as a failure in **Locust**’s statistics. The returned dummy Response’s content attribute
will be set to None, and its _status_code_ will be 0.


### Start Locust
To run **Locust** with a _locustfile_, if it was named **_locustfile.py_** and located in the current working directory,
we could run:

```bash
$ locust --host=http://example.com
```


If the _locustfile_ is located under a subdirectory and/or named different than **_locustfile.py_**, specify it using
`-f`:

```bash
$ locust -f locust_files/my_locust_file.py --host=http://example.com
```

To run Locust distributed across multiple processes we would start a master process by specifying `--master`:

```bash
$ locust -f locust_files/my_locust_file.py --master --host=http://example.com
```

and then we would start an arbitrary number of slave processes:

```bash
$ locust -f locust_files/my_locust_file.py --slave --host=http://example.com
```

If we want to run **Locust** distributed on multiple machines we would also have to specify the master host when
starting the slaves (this is not needed when running Locust distributed on a single machine, since the master host
defaults to 127.0.0.1):

```bash
$ locust -f locust_files/my_locust_file.py --slave --master-host=192.168.0.100 --host=http://example.com
```

### Common libraries
Often, people wish to group multiple _locustfile_ that share common libraries. In that case, it is important to define
the project root to be the directory where you invoke `locust`, and it is suggested that all _locustfile_ live
somewhere beneath the project root.

Subdirectories can be a cleaner approach, but `locust` will only import modules relative to the directory in which the
running _locustfile_ is placed. If you wish to import from your project root, make sure to write
`sys.path.append(os.getcwd())` in your _locustfile_(s) before importing any common libraries —this will make the
project root importable.

```text
project_root/
    __init__.py
    common/
        __init__.py
        config.py
        auth.py
    locustfiles/
        __init__.py
        web_app.py
        api.py
        ecommerce.py
```

With the above project structure, your _locustfile_ can import common libraries using:

```python
import sys
import os
sys.path.append(os.getcwd())
import common.auth
```

**Note**: To see all available options type: `locust --help`.

## Resources
 - [Locust docker image](https://github.com/karol-brejna-i/locust-experiments/tree/master/docker-image)
    - [docker hub](https://hub.docker.com/r/grubykarol/locust)
    - [Dockerfile](https://github.com/karol-brejna-i/docker-locust)
 - [mqtt-locust](https://github.com/ajm188/mqtt-locust)
    - [mqtt-locust - AWS IoT](https://github.com/concurrencylabs/mqtt-locust)